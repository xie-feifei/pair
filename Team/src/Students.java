import java.util.Comparator;

public class Students implements Comparator<Students>{
	private String name;
	private String id;
	private double exp;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getExp() {
		return exp;
	}

	public void setExp(double exp) {
		this.exp = exp;
	}

	@Override
	public String toString() {
		return  id + ", " + name + ", " + exp;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(exp);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Students other = (Students) obj;
		if (Double.doubleToLongBits(exp) != Double.doubleToLongBits(other.exp))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Students(String id, String name, double exp) {
		super();
		this.id = id;
		this.name = name;
		this.exp = exp;
	}
	public Students() {
		super();
	}

	@Override
	public int compare(Students o1, Students o2) {
		// TODO Auto-generated method stub
		int sort1 = Integer.parseInt(o1.getId()) - Integer.parseInt(o2.getId());
		int sort2 = (int) (o2.getExp() - o1.getExp());
		return sort2==0?sort1:sort2;
	}
}
	

