import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {
	// 网址
	public static String URL;
	// cookie信息
	public static String COOKIE;

	public static void main(String[] args) throws IOException {
		// 读取源文件的url和cookie
		getResources();

	}

	private static void getResources() {
		// 从配置文件中加载配置项：使用Properties的load()方法
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("resources/config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 读取配置项中的结果：使用Properties的getProperty()方法
		URL = prop.getProperty("url");
		COOKIE = prop.getProperty("cookie");
	}
	
	//获取所有课堂完成部分的url 
	public void setCookies(String url) throws IOException {
		// 手动设置cookies
		Document document = Jsoup.connect(URL).header("Cookie", COOKIE).get();
		
		if (document != null) {
			// 获取所有活动的div
			Elements element = document.getElementsByClass("interaction-row");
			// 初始化链表
			ArrayList<String> baseList = new ArrayList<String>();
			for (int i = 0; i < element.size(); i++) {
				if (element.get(i).child(1).child(0).child(1).text().indexOf("课堂完成") != -1) {
					// 将属性data-url的值转为字符串
					String urlString = element.get(i).attr("data-url").toString();
					// 把值加到baseList
					baseList.add(urlString);
				}
			}
		}
	}
	
	public static ArrayList<Students> stuList(ArrayList<String> baseList) throws IOException{
		//初始化一个列表存储解析课堂完成部分的html文件的document对象
		ArrayList<Document> baseActives = new ArrayList<Document>(baseList.size()); 
		//学生列表(包含每个活动的每个学生）
		ArrayList<Students> stuList = new ArrayList<>();
		//存储解析课堂完成部分的html文件的document对象
		for(int i=0;i<baseList.size();i++) {
			//解析课堂完成部分
			Document document = Jsoup.connect(baseList.get(i)).header("cookie", COOKIE).get();
			//将解析后的document添加到baseActives
			baseActives.add(document);
		}
		for(int j=0;j<baseActives.size();j++) {
			//获取每个课堂完成部分网页里的class="homework-item"的div
			Elements baseStuDivs = baseActives.get(j).getElementsByClass("homework-item");
			for(int k=0;k<baseStuDivs.size();k++) {
				try {
					//学生
					Students stu = new Students();
					//设置学号
					stu.setId(baseStuDivs.get(k).child(0).child(1).child(1).text().toString());
					//设置姓名
					stu.setName(baseStuDivs.get(k).child(0).child(1).child(0).text().toString());
					//获得成绩文本
					String score = baseStuDivs.get(k).child(3).child(1).child(1).text();
					//没评分的分数为0
					if(baseStuDivs.get(k).child(3).child(0).child(1).text().contains("尚无评分")) {
						stu.setExp(0.0);
					}
					//没提交的分数为0
					else if (baseStuDivs.get(k).child(1).child(0).text().contains("未提交")) {
						stu.setExp(0.0);
					}
					else {
						//设置成绩
						stu.setExp(Double.parseDouble(score.substring(0, score.length() - 2)));
					}
					stuList.add(stu);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		// 去重后的学生列表
				ArrayList<Students> newStudentList = new ArrayList<>();
				// 每个学生的总成绩
				Double totalScore;
				for (int i = 0; i < stuList.size(); i++) {
					// 初始化每个学生的总成绩
					totalScore = 0.0;
					// 学生
					Students student = new Students();
					for (int j = i + 1; j < stuList.size(); j++) {
						if (stuList.get(i).getName().contains(stuList.get(j).getName())) {
							// 计算成绩
							totalScore += stuList.get(j).getExp();
							// 删掉重复的
							stuList.remove(j);
						}
					}
					// 学生和学生的总成绩的学生对象
					student.setId(stuList.get(i).getId());
					student.setName(stuList.get(i).getName());
					student.setExp(totalScore);
					// 加入到新列表
					newStudentList.add(student);
				}
				return newStudentList;
			}
	public static void write(ArrayList<Students> newStudentList) throws FileNotFoundException {
		// 确定输出文件的目的地
		File file = new File("score.txt");
		// 创建指向文件的打印输出流
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file), true);
		// 输出数据
		double ave = 0.0;
		for (int j = 0; j < newStudentList.size(); j++) {
			ave += newStudentList.get(j).getExp();
		}
		ave = ave / newStudentList.size();
		printWriter.println("最高经验值" + newStudentList.get(0).getExp() + ",最低经验值"
				+ newStudentList.get(newStudentList.size() - 1).getExp() + ",平均经验值" + ave);
		for (int i = 0; i < newStudentList.size(); i++) {
			printWriter.println(newStudentList.get(i).toString());
		}
		// 关闭
		printWriter.close();
	}
}